# Class: hiera_user
#
# This module manages hiera_user
#
# Parameters: none
#
# Actions:
#
# Requires: see Modulefile
#
# Sample Usage:
#
define hiera_user ($name = $title, $group, $key, $password) {
  
  # group creation if not in catalog
  if ! defined(Group[$group]){
    group{$group:}
  }
  
  # user creation if not in catalog
  if ! defined(User[$name]){
    user{$name:
      gid => $group,
      password => sha1($password),
      managehome => true
    }
  }
  
  # add key
  
  ssh_authorized_key{"${name} ssh key":
    key => $key,
    user => $name
  }

  
}
